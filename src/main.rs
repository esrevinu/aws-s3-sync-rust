use rusoto_core::ByteStream;
use rusoto_s3::{DeleteObjectRequest,
                ListObjectsV2Request,
                PutObjectRequest,
                S3Client,
                S3};
use std::any::type_name;
use std::collections::HashMap;
use std::env;
use std::fmt::Display;
use std::fs;
use std::io;
use std::path::Path;
use mime_detective::MimeDetective;

#[derive(Debug)]
struct MyError {
    message: String,
}

impl<T: Display> From<T> for MyError {
    fn from(t: T) -> Self {
        Self { message: format!("{}: {}", type_name::<T>(), t), }
    }
}

struct SyncCheck {
    local_md5: Vec<u8>,
    local_etag: String,
    cloud_etag: String,
}

fn main() -> Result<(),MyError> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        eprintln!("number of arguments is less than 2!");
        eprintln!("Usage: {} <local dir> <bucket name>", args[0]);
        return Err({ "Argument Error".into() });
    }
    let from_dir = &args[1];
    let to_bucket = &args[2];

    let mut hmap: HashMap<String, SyncCheck> = HashMap::new();

    // Cloud
    let client = S3Client::new(Default::default());
    let list_bucket_req: ListObjectsV2Request = ListObjectsV2Request {
        bucket: to_bucket.to_string(),
        ..Default::default()
    };
    let output = client.list_objects_v2(list_bucket_req).sync()?;
    for obj in output.contents.unwrap() {
        hmap.insert(obj.key.unwrap(), SyncCheck {
            local_md5: Vec::new(),
            local_etag: String::default(),
            cloud_etag: obj.e_tag.unwrap(),
        });
    }

    // Local
    env::set_current_dir(Path::new(&from_dir))?;
    visit_dirs(Path::new("."), &mut hmap)?;

    // Sync
    for (k, v) in hmap {
        if v.local_etag == String::default() {
            delete_object(&client, to_bucket.to_string(), k)?;
        } else if v.cloud_etag == String::default() ||
            v.cloud_etag != v.local_etag {
            put_object(&client, to_bucket.to_string(), k, &v)?;
        }
    }
    Ok(())
}

fn delete_object(client: &S3Client, bucket: String, key: String)
                 -> Result<(),MyError> {
    let delete_object_req: DeleteObjectRequest = DeleteObjectRequest {
        bucket: bucket,
        key: key.to_string(),
        ..Default::default()
    };
    println!("DeleteObject: {}", key);
    client.delete_object(delete_object_req).sync()?;
    Ok(())
}

fn put_object(client: &S3Client, bucket: String,
              key: String, sync_check: &SyncCheck) -> Result<(),MyError> {
    let md5_base64 = base64::encode(&sync_check.local_md5);
    let f = ByteStream::from(fs::read(&key)?);
    let ct = mime_detect(&key);
    let put_object_req: PutObjectRequest = PutObjectRequest {
        bucket: bucket,
        key: key.to_string(),
        body: Some(f),
        content_md5: Some(md5_base64),
        content_type: Some(ct.to_string()),
        ..Default::default()
    };
    println!("PutObject: {} Type: {} MD5: {} ",
             key, ct, sync_check.local_etag);
    client.put_object(put_object_req).sync()?;
    Ok(())
}

fn mime_detect(path: &String) -> String {
    let ct = mime_guess::from_path(path).first_or_octet_stream();
    if ct == mime::APPLICATION_OCTET_STREAM {
        let detective = MimeDetective::new();
        match detective {
            Err(_) => ct,
            Ok(d)  => {
                let mime = d.detect_filepath(path);
                match mime {
                    Err(_) => ct,
                    Ok(m)  => m,
                }
            }
        }.to_string()
    } else {
        ct.to_string()
    }
}

fn visit_dirs(dir: &Path, mut hmap: &mut HashMap<String, SyncCheck>)
              -> Result<(),MyError> {
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                visit_dirs(&path, &mut hmap)?;
            } else {
                let file = path.strip_prefix("./")?;
                let key = file.to_str().unwrap().to_string();
                match hmap.get_mut(&key) {
                    Some(v) => {
                        let (md5, etag) = md5_local(&path)?;
                        v.local_md5 = md5;
                        v.local_etag = etag;
                    },
                    None => {
                        let (md5, etag) = md5_local(&path)?;
                        hmap.insert(key, SyncCheck {
                            local_md5: md5,
                            local_etag: etag,
                            cloud_etag: String::default(),
                        });
                    },
                }
            }
        }
    }
    Ok(())
}

fn md5_local(file: &Path) -> io::Result<(Vec<u8>,String)> {
    let data = &fs::read(file)?;
    let digest = md5::compute(data);
    Ok((digest.to_vec(), format!("\"{:x}\"", digest)))
}
